# Pill Clan Quake Live Server Information
Information about the Pill Clan Quake Live Freeze Tag (FT) Server for my mostly
personal use.

Compiled by `Ra1n`. Server owned and operated (AFAIK) by `lint`.

# Server Website
http://chicago.pillclan.com

# Discord Address
http://discord.pillclan.com

# Server Address
Main FT Server: `chicago.pillclan.com:27960` (QLStats:
https://qlstats.net/server/662)  

Alt FT Server: `chicago2.pillclan.com:27960`  

Instagib FT (iFT) Server: `chicago2.pillclan.com:27961`  

CTF Server: `chicago.pillclan.com:27963` (QLStats:
https://qlstats.net/server/956)  

# Map List
These are some maps that are **supposed** to be on the server currently:  
[lint's List](maps-lint.md)   

[DevilDoc's List](maps-doc.md) 
([Spreadsheet Direct
Link](https://docs.google.com/spreadsheets/d/1yul1fc_MPGY6BsMEGRnVmllKYkLvJo3RqTS_rZVBUA0/edit#gid=203934706))  

[Ra1n's List](maps-ra1n.md) (Pruned from above, plus a few additions)  


# Things to do  
 - None atm.
